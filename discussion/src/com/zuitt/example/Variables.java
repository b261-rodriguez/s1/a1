// A package in java is used to group related classes. Thuink of it as a folder in directory
    // Packages are divided into two categories
        //1. Built-in Packages - (Packages from JAVA API
        //2. User-defined Packages

// Packages creation follow th "reverse domain name notation" for the naming convention

package com.zuitt.example;

public class Variables {

    public static void main(String[] args){

//        Variables
        int age;
        char middleInitial;

//        Variables Declaration vs. Initialization
        int x;
        int y = 0;

//        initialization after declaration

        x = 1;

        System.out.println("The value of y is " + y + " and the value of x is " + x);

//        Primitive Data types
            // Predefined within the java programming which is used for a "single-valued variables with limited capabilities

        // int - whole number values

        int wholeNumber = 100;
        System.out.println(wholeNumber);
        // Long
        // L is being added at the end of the long number to be recognized
        long worldPopulation = 7685154148556L;
        System.out.println(worldPopulation);

        //float
        // add f at the end of the float to be recognize
        float piFloat = 3.141512311125132f;
        System.out.println(piFloat);

        //double
        double piDouble = 3.141512311125132;
        System.out.println(piDouble);

        //char - single characters
        // uses single quote
        char letter = 'a';
        System.out.println(letter);

        // boolean true or false;

        boolean isLove = true;
        boolean isTaken = false;

        System.out.println(isLove);
        System.out.println(isTaken);

        // Constants

        final int PRINCIPAL = 300;

        System.out.println(PRINCIPAL);

        // Non Primitive data type
        // also known as reference data types refer to instances or objects

        // String
        // Stores a sequences or array of characters
        // String are actually object that can use methods

        String userName = "JSmith";
        System.out.println(userName);

        int stringLength = userName.length();
        System.out.println(stringLength);






    }

}
